package com.company.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;
import java.util.Scanner;
import java.io.IOException;


public class Player {
  private static Logger log = LogManager.getFormatterLogger(Message.class.getName());
  private Client client = new Client();
  private Scanner consoleInput = new Scanner(System.in);
  private Message msg = new Message();

  public void getServerAddress()
  {
    // System.out.println("host");
    // String host = consoleInput.nextLine();
    // System.out.println("port");
    // String port = consoleInput.nextLine();

    String host = "35.163.138.220";
    //String host = "127.0.0.1";
    String port = "12001";
    if (client.Initialize()) {
      client.setConnection(host + ":" + port);
    }
    else {
      System.out.println("did not initialize connection correctly");
      log.error("FATAL ERROR: did not initialize connection correctly");
      System.exit(0);
    }
  }
  public String getCommand()
  {
      System.out.println("G=Guess, H=Hint, E=Exit");
      String GHE = consoleInput.nextLine();

      return GHE;
  }
  public void startGame()
  {
      // System.out.println("A#:");
      // String A = consoleInput.nextLine();
      // System.out.println("Last Name:");
      // String LN = consoleInput.nextLine();
      // System.out.println("First Name:");
      // String FN = consoleInput.nextLine();
      // System.out.println("Alias:");
      // String Alias = consoleInput.nextLine();
      String A = "A01503588";
      String LN = "Yeagley";
      String FN = "J";
      String Alias = "Jake";

      Short MT = 1;
      try {
        ByteBuffer bb = msg.encodeNewGame(A, LN, FN, Alias);
        client.sendMessageToServer(bb);
      } catch (IOException ex)
      {
        log.error("exception was thrown in startGame. Could not send message to server");
      }

  }
  public void guess()
  {
      System.out.println("Guess:");
      String guess = consoleInput.nextLine();

      try {
        ByteBuffer bb = msg.encodeGuess(guess);
        client.sendMessageToServer(bb);
      } catch (IOException ex)
      {
        log.error("exception was thrown in guess. Could not send message to server");
      }
  }
  public void hint()
  {
      try {
          ByteBuffer bb = msg.encodeHint();
          client.sendMessageToServer(bb);
      } catch (IOException ex)
      {
          log.error("exception was thrown in hint. Could not send message to server");
      }
  }
  public boolean exit()
  {
      try {
          ByteBuffer bb = msg.encodeExit();
          client.sendMessageToServer(bb);
      } catch (IOException ex)
      {
          log.error("exception was thrown in exit. Could not send message to server");
      }
      System.exit(0);
      return true;
  }

}
