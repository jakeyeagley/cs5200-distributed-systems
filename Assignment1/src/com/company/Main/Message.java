package com.company.Main;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Message {
    private static Logger log = LogManager.getFormatterLogger(Message.class.getName());
    private static short game_id;

    public Message() {

    }

    public List<String> decodeMessageType(ByteBuffer bytes, DatagramChannel myUdpClient, Client client)
    {
        log.info("decoding message type");
        if (bytes == null)
        {
            log.error("error in decoding message type bytes where null");
        }

        bytes.order(ByteOrder.BIG_ENDIAN);

        short mt = bytes.getShort();
        log.info("message type was " + mt);

        if (mt == 10){
            try{
                sendHeartbeat(client);
            } catch (IOException ex){
                log.error("exception was thrown in decodeMessageType. Could not start heartbeat");
            }
            return null;
        }
        else {
            return _getMessage(bytes, mt);
        }
    }

    /*
    return an array of strings
    [0] = message type
    [1] = gameDef
    [2] = hint length
    [3] = hint
     */
    public static List<String> decodeGameDef(ByteBuffer bytes, int mt)
    {
      if (mt != 2) {
          log.debug("received wrong message type. Received " + mt + " expected 2");
          return null;
      }
      short id = bytes.getShort();

      short hint_length = bytes.getShort();
      if (bytes.remaining() < hint_length) {
        log.error("in decodeGameDef the byte array is too short for hint text");
        return null;
      }

      byte[] hintBytes = new byte[hint_length];
      bytes.get(hintBytes, 0, hint_length);
      String hintText = new String(hintBytes);

      for (int i = 0; i < hintText.length(); i++)
      {
          hintText = removeByIndex(hintText,i);
      }

      short def_length = bytes.getShort();

      if (bytes.remaining() < def_length) {
          log.error("in decodeGameDef the byte array is too short for def text");
          return null;
      }

      byte[] textBytes = new byte[def_length];
      bytes.get(textBytes, 0, def_length);
      String defText = new String(textBytes);

      for (int i = 0; i < defText.length(); i++)
      {
         defText = removeByIndex(defText,i);
      }

      game_id = id;
      List<String> gameDef = new ArrayList<String>();
      gameDef.add(Integer.toString(mt));
      gameDef.add(defText);
      gameDef.add(Integer.toString(hintText.length()));
      gameDef.add(hintText);

      return gameDef;
    }

    /*
    [0] = message type
    [1] = won
    [2] = score
    [3] = hint
     */
    public static List<String> decodeAnswer(ByteBuffer bytes, int mt){
       if (mt != 4){
           log.debug("received wrong message type. Received " + mt + " expected 4");
           return null;
       }

       short id = bytes.getShort();
       if (id != game_id){
           log.debug("Wrong id .Expected " + game_id + " got " + id);
           return null;
       }

       byte won = bytes.get();

       short score = bytes.getShort();

       short hint_length = bytes.getShort();
       byte[] textBytes = new byte[hint_length];
       bytes.get(textBytes, 0, hint_length);
       String hintText = new String(textBytes);

       for (int i = 0; i < hintText.length(); i++)
       {
          hintText = removeByIndex(hintText,i);
       }

       List<String> gameDef = new ArrayList<String>();
       gameDef.add(Integer.toString(mt));
       gameDef.add(Byte.toString(won));
       gameDef.add(Short.toString(score));
       gameDef.add(hintText);

       return gameDef;
    }
    /*
    [0] = message type
    [1] = hint
     */
    public static List<String> decodeHint(ByteBuffer bytes, int mt){
        if (mt != 6){
            log.debug("received wrong message type. Received " + mt + " expected 6");
            return null;
        }

        short id = bytes.getShort();
        if (id != game_id){
            log.debug("Wrong id. Expected " + game_id + " got " + id);
            return null;
        }

        short hint_length = bytes.getShort();
        byte[] textBytes = new byte[hint_length];
        bytes.get(textBytes, 0, hint_length);
        String hintText = new String(textBytes);

        for (int i = 0; i < hintText.length(); i++)
        {
           hintText = removeByIndex(hintText,i);
        }

        List<String> gameDef = new ArrayList<String>();
        gameDef.add(Integer.toString(mt));
        gameDef.add(hintText);

        return gameDef;

    }

    public ByteBuffer encodeNewGame(String ANum, String LastName, String FirstName, String Alias) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        short messageType = 1;

        ByteBuffer mt = encodeShortInt(messageType);
        ByteBuffer an = encodeString(ANum);
        ByteBuffer ln = encodeString(LastName);
        ByteBuffer fn = encodeString(FirstName);
        ByteBuffer A = encodeString(Alias);

        outputStream.write(mt.array());
        outputStream.write(an.array());
        outputStream.write(ln.array());
        outputStream.write(fn.array());
        outputStream.write(A.array());

        log.info("Encoded newGame ByteBuffer " + ByteBuffer.wrap(outputStream.toByteArray()));
        return ByteBuffer.wrap(outputStream.toByteArray());
    }

    public ByteBuffer encodeGuess(String guess) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        short messageType = 3;
        ByteBuffer mt = encodeShortInt(messageType);
        ByteBuffer id = encodeShortInt(game_id);
        ByteBuffer g = encodeString(guess);

        outputStream.write(mt.array());
        outputStream.write(id.array());
        outputStream.write(g.array());

        log.info("Encoded guess ByteBuffer " + ByteBuffer.wrap(outputStream.toByteArray()));
        return ByteBuffer.wrap(outputStream.toByteArray());
    }

    public ByteBuffer encodeHeartbeat() throws IOException
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        short messageType = 8;
        ByteBuffer mt = encodeShortInt(messageType);
        ByteBuffer id = encodeShortInt(game_id);

        outputStream.write(mt.array());
        outputStream.write(id.array());

        return ByteBuffer.wrap(outputStream.toByteArray());
    }

    public ByteBuffer encodeHint() throws IOException{
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        short messageType = 5;
        ByteBuffer mt = encodeShortInt(messageType);
        ByteBuffer id = encodeShortInt(game_id);

        outputStream.write(mt.array());
        outputStream.write(id.array());

        log.info("Encoded hint ByteBuffer " + ByteBuffer.wrap(outputStream.toByteArray()));
        return ByteBuffer.wrap(outputStream.toByteArray());
    }

    public ByteBuffer encodeExit() throws IOException{
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        short messageType = 7;
        ByteBuffer mt = encodeShortInt(messageType);
        ByteBuffer id = encodeShortInt(game_id);

        outputStream.write(mt.array());
        outputStream.write(id.array());

        log.info("Encoded exit ByteBuffer " + ByteBuffer.wrap(outputStream.toByteArray()));
        return ByteBuffer.wrap(outputStream.toByteArray());
    }

    public ByteBuffer encodeString(String str) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte[] textBytes = str.getBytes(StandardCharsets.UTF_16);
        ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putShort((short) textBytes.length);
        outputStream.write(buffer.array());
        outputStream.write(textBytes);

        return ByteBuffer.wrap(outputStream.toByteArray());
    }

    public ByteBuffer encodeShortInt(short sint) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
        // follow the Big Endian byte order
        buffer.order(ByteOrder.BIG_ENDIAN);
        buffer.putShort(sint);
        outputStream.write(buffer.array());

        return ByteBuffer.wrap(outputStream.toByteArray());
    }

    //stackoverflow
    private static String removeByIndex(String str, int index) {
        StringBuilder sb = new StringBuilder();
        if (index > 0) {
            sb.append(str, 0, index);
        }
        if (index < str.length() - 1) {
            sb.append(str, index + 1, str.length());
        }
        return sb.toString();
    }

    private static List<String> _getMessage(ByteBuffer bytes, int messageType)
    {
        if (bytes == null) return null;

        List<String> message = null;
        if (messageType == 2){
            message = Message.decodeGameDef(bytes, messageType);
        }
        else if (messageType == 4){
            message = Message.decodeAnswer(bytes, messageType);
        }
        else if (messageType == 6){
            message = Message.decodeHint(bytes, messageType);
        }

        if (message == null) return null;
        if (!message.get(0).equals(Integer.toString(messageType))) {
            log.error("Wrong message type recieved in client. Expected " + messageType + " got " + message.get(0));
            return null;
        }

        return message;
    }

    private void sendHeartbeat(Client client) throws IOException {
        ByteBuffer bb = encodeHeartbeat();
        client.sendMessageToServer(bb);
    }

}
