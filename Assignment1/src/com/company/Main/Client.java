package com.company.Main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;


public class Client {
  private static Logger log = LogManager.getFormatterLogger(Client.class.getName());
  private static InetSocketAddress serverAddress;
  private DatagramChannel myUdpClient;

  public void setConnection(String address)
  {
    serverAddress = EndPointParser.Parse(address);
    if (serverAddress == null)
    {
      log.error("FATAL ERROR: connection refused");
    }
    Receiver receiver = new Receiver(myUdpClient, this);
    new Thread(() -> {
         receiver.listen();
    }).start();
  }

  public void sendMessageToServer(ByteBuffer bytesToSend) {
      //DisplayEndPoints();

      try {
          int result = myUdpClient.send(bytesToSend, serverAddress);
          // use result for debugging
          log.info("sent " + result + " bytes");
      } catch (Exception ex) {
          log.error("exception sending message to server " + ex);
      }
  }

  public boolean Initialize() {
      log.debug("Create UdpClient");

      boolean result = false;
      try {
          myUdpClient = DatagramChannel.open();
          InetSocketAddress localEndPoint = new InetSocketAddress("0.0.0.0", 0);
          myUdpClient.bind(localEndPoint);
          result = true;
      }
      catch (IOException ex) {
        log.error("Cannot setup UDP Client: %s" + ex.getMessage());
      }
      return result;
  }

}
