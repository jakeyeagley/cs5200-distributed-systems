package com.company.Main;

import java.net.InetSocketAddress;

public class EndPointParser {

    public static InetSocketAddress Parse(String hostnameAndPort)
    {
        if (isNullOrWhiteSpace(hostnameAndPort)) return null;

        String[] parts = hostnameAndPort.split(":");

        if (parts.length != 2) return null;

        String hostAddress = parts[0].trim();
        String port = parts[1].trim();
        if (hostAddress.isEmpty() || port.isEmpty()) return null;

        return new InetSocketAddress(hostAddress, Integer.parseInt(port));
    }

    private static boolean isNullOrWhiteSpace(String value) {
        return value==null || value.trim().isEmpty();
    }
}
