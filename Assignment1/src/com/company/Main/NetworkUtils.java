package com.company.Main;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class NetworkUtils {

    public static List<String> getIpAddresses() {

        List<String> addresses = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();

                if (networkInterface.getInterfaceAddresses().size() <= 0 ||
                        networkInterface.getDisplayName().toLowerCase().contains("virtual")) continue;


                Enumeration<InetAddress> interfaceAddresses = networkInterface.getInetAddresses();
                while (interfaceAddresses.hasMoreElements()) {
                    InetAddress inetAddress = interfaceAddresses.nextElement();
                    if (inetAddress instanceof Inet4Address)
                        addresses.add(inetAddress.getHostAddress());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return addresses;
    }

}
