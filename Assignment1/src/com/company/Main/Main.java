package com.company.Main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
  private static Logger log = LogManager.getFormatterLogger(Message.class.getName());
  public static void main(String[] args)
  {
    Player player = new Player();
    player.getServerAddress();
    player.startGame();

    boolean run = true;
    while(run)
    {
      String GHE = player.getCommand();
      log.info("User input was " + GHE);

      if (GHE.equals("G") || GHE.equals("g")){
        player.guess();
      }
      if (GHE.equals("H") || GHE.equals("h")){
        player.hint();
      }
      if (GHE.equals("E") || GHE.equals("e")){
        player.exit();
        run = false;
      }
    }
  }
}