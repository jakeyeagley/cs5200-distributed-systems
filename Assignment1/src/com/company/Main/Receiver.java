package com.company.Main;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Receiver {
    private static Logger log = LogManager.getFormatterLogger(Message.class.getName());
    private DatagramChannel myUdpClient;
    private List<String> gameDef = null;
    private Client client;
    private Message message = new Message();
    private String lastHint = "";

    public Receiver(DatagramChannel udpClient, Client cl){
       myUdpClient = udpClient;
       client = cl;
    }
    public void listen() {

        boolean quit = false;
        log.info("waiting for message");
        while (!quit)
        {
            ByteBuffer buffer = ByteBuffer.allocate(2048);

            try{
                SocketAddress remoteEndPoint = myUdpClient.receive(buffer);
                if (remoteEndPoint == null) {
                    try {
                        Thread.sleep(0);
                    }
                    catch (Exception ex) {
                        log.error("Exception on thread in listen " + ex.getMessage());
                    }
                    continue;
                }
                buffer.flip();

                gameDef = message.decodeMessageType(buffer, myUdpClient, client);
                if (gameDef != null){
                    quit = true;
                }
            }
            catch (IOException ex) {
                log.error("exception while listening for a message " + ex.getMessage());
            }
        }
        handleReceivedMessage();
        gameDef = null;
        listen();
    }

    private void handleReceivedMessage()
    {
        log.info("Message received " + gameDef);
        // start of game
        if (gameDef.get(0).equals("2"))
        {
            System.out.println("Starting a new game");
            System.out.println("Your hint is: " + gameDef.get(1));
            System.out.println("The word is " + gameDef.get(2) + " characters long");
            System.out.println(gameDef.get(3));
        }
        // guess
        if (gameDef.get(0).equals("4"))
        {
            System.out.println("SCORE: " + gameDef.get(2));
            if (gameDef.get(1).equals("1"))
            {
                System.out.println("You Won");
                //exit
                System.exit(0);
            }
            if (!gameDef.get(3).contains("_"))
            {
                System.out.println("You Lose");
                System.out.println("the word was " + gameDef.get(3));
                System.exit(0);
            }
            System.out.println(gameDef.get(3));
        }
        // hint
        if (gameDef.get(0).equals("6"))
        {
            if (!gameDef.get(1).contains("_"))
            {
                System.out.println("You Lose");
                System.exit(0);
            }
            lastHint = gameDef.get(1);
            System.out.println(gameDef.get(1));
        }
    }

}
