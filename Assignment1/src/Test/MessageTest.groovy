package Test

import com.company.Main.Message

import java.nio.ByteBuffer

class MessageTest extends groovy.util.GroovyTestCase {
    void testEncodeString() {
        //set up
        String test = "test"
        Message message = new Message()

        //execute
        ByteBuffer bb = message.encodeString(test)

        //compare
        assertEquals(12, bb.remaining())
        assertEquals(0, bb.array()[0])
        assertEquals(10, bb.array()[1])
        assertEquals(-2, bb.array()[2])
        assertEquals(-1, bb.array()[3])
        assertEquals(0,bb.array()[4])
        assertEquals(116, bb.array()[5])
    }

    void testEncodeShortInt() {
        //set up
        short test = 5
        Message message = new Message()

        //execute
        ByteBuffer bb = message.encodeShortInt(test)

        //compare
        assertEquals(2, bb.remaining())
        assertEquals(5, bb.array()[1])
    }

    void testEncodeNewGame() {
        String anum = "A0000000"
        String ln = "Doe"
        String fn = "Johnathan"
        String alias = "John"

        Message message = new Message();

        ByteBuffer bb = message.encodeNewGame(anum, ln, fn, alias);
        assertEquals(66, bb.remaining())
    }

    void testEncodeGuess() {
        String guess = "something"

        Message message = new Message();

        ByteBuffer bb = message.encodeGuess(guess);
        assertEquals(26, bb.remaining())
    }

    void testEncodeHeartbeat() {
        Message message = new Message();

        ByteBuffer bb = message.encodeHeartbeat();
        assertEquals(4, bb.remaining())
    }

    void testEncodeHint(){
        Message message = new Message();

        ByteBuffer bb = message.encodeHint();

        assertEquals(4, bb.remaining());
    }

    void testEncodeExit() {
        Message message = new Message();

        ByteBuffer bb = message.encodeExit()

        assertEquals(4, bb.remaining());
    }
}
