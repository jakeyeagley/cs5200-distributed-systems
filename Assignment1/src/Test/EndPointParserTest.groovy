import com.company.Main.EndPointParser

class EndPointParserTest extends groovy.util.GroovyTestCase {
    void testParse() {
        EndPointParser parser = new EndPointParser();
        String test1 = " ";
        String test2 = null;
        String test3 = "127.0.0.1";
        String test4 = "127.0.0.1:12001";

        InetSocketAddress result1 = parser.Parse(test1);
        InetSocketAddress result2 = parser.Parse(test2);
        InetSocketAddress result3 = parser.Parse(test3);
        InetSocketAddress result4 = parser.Parse(test4);


        assertNull(result1);
        assertNull(result2);
        assertNull(result3);
        assertNotNull(result4);

        InetSocketAddress compare = new InetSocketAddress("127.0.0.1", 12001)
        assertEquals(result4, compare);
    }
}
